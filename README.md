# Liboutput

It would be nice if there were a shared library that sat atop DRM/KMS
and exposed an API suitable for writing compositing managers and
direct applications.

## Related Work

### Libliftoff

[Libliftoff](https://github.com/emersion/libliftoff) “eases the use of
KMS planes from userspace without standing in your way”. This does the
hard work of dealing with the DRM/KMS atomic mode setting API for
getting images blended using the hardware scanout engine.

Libliftoff doesn't (currently) include any of the fall-back rendering
required for dealing with limitations in the hardware, like not enough
overlays, format conversions and unsupported transformations.

I think it's worth exploring whether libliftoff should adopt a broader
set of goals, including some of the ones listed here. If so, I think
that libliftoff could be a good place to do all of this work. If not,
then liboutput would sit atop libliftoff.

### wlroots

[wlroots](https://github.com/swaywm/wlroots) is “A modular Wayland
compositor library”. It offers a huge amount of code designed to
simplify the implementation of a Wayland compositor.

Parts of wlroots are driving DRM/KMS, and parts are supporting Wayland
protocols. The bits which are for DRM/KMS could be re-worked to sit
atop a higher level API. I also imagine that the accelerated
compositing code would be useful in building something usable by
non-Wayland systems.

## Goals

Let's start by listing a few goals

### Support existing applications

 * X server
 * Weston
 * Kwin
 * Mutter
 * EGL
 * Vulkan display

### Drive multiple planes

We want to take full advantage of sophisticated scanout hardware,
which means using at least multiple planes in different formats.

### Internal rendering paths for fallbacks

Applications should be able to submit arbitrary numbers of planes in
arbitrary formats and have liboutput handle any necessary rendering
steps to prepare them for scanout.

### Leasing

Liboutput needs to be able to build a KMS lease

### Transformations

I think we'll want projective transforms.

### Output Support

This comes from the application support goals, but we can enumerate
them more completely here

 * Multiple outputs
 * Mode enumeration
 * Custom mode creation
 * Prime modes

### Soft outputs

We want to offer software-defined outputs for testing and
virtulization.

### Scanout buffer rendering

Immediate-mode applications, like the X server, need to be able to
draw directlty to scanout buffers.

### Display Timing

Vulkan and GL APIs both expose APIs to control when application
presentation occurs, and Vulkan has APIs for getting feedback about
presentation back to applications. These are invaluable for developers
with strong presentation timing requirements.

## Non-Goals

As important as what we want, a list of things we definitely don't
want to include here

### Network Protocol

liboutput is not a compositing server, it's a library that makes
writing applications that want to use DRM/KMS easier.

### Rendering

While liboutput will need to use the rendering system for compositing,
we don't want an API for arbitrary rendering within liboutput

### Support for non DRM/KMS devices

Trying to support non DRM/KMS devices means adding a lot of
abstraction on top of the DRM/KMS objects. I think that will make the
API much deeper and harder to understand.

## Architecture Ideas

These are here just to provide a framework for discussion about the
goals; nothing here is in any way fixed.

### Stack of Images

Applications will pass liboutput a stack of images. This stack may
be displayed on one or more outputs. Each image may have a projective
transformation applied to it. Images with internal alpha values may
use them (or not). Images may also have a global alpha value
applied. I think you get to pick one of these two modes, not both.

### Atomic Presentation

Applications will submit the whole stack of images (for multiple
displays?) in a single call. Liboutput will re-compute the
presentation, composite images together and submit the result to KMS.

### Describing Displays

The KMS properties for each display will be exposed to
applications. Planes, CRTCs and other elements of the KMS API will be
hidden from the application and left to liboutput to manage.

### Partial updates

When an application draws to a scanout buffer, it may need to send a
'damage' region to liboutput to drive redisplay. Liboutput should
tell the application whether this is necessary to allow applications
to avoid doing damage computation.

### Presentation feedback

Liboutput will tell applications (synchronously) when presentation has
occurred, and should be able to provide a fine-grained timestamp of
the precise time when the first pixel went out the connector.

### Device-specific plugins

Because many constraints of output devices are not well explained by
the kinds of feedback you can get from KMS, it might be useful to
have device-specific code to help select and manage output resources.
